<?php

Route::post('/', function () {
    collect(request('emails'), [])->each(function ($emailRecipient) {
        Mail::send('emails.example', ['emailRecipient' => $emailRecipient], function ($m) use ($emailRecipient) {
            $m->to($emailRecipient);
            $m->subject('This is a testing email!');
            $m->from('noreply@enolalabs.com');
            $m->bcc('hidden@enolalabs.com');
            $m->cc('copy@enolalabs.com');
        });
    });
});
