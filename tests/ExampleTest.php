<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use MailThief\Facades\MailThief;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        MailThief::hijack();

        $emailRecipients = [
            'example1@enolalabs.com',
            'example2@enolalabs.com'
        ];
        $this->call('POST', '/', [
            'emails' => $emailRecipients
        ]);

        $mailThief = Mail::getFacadeRoot();
        $this->assertEquals(2, $mailThief->messages->count(), 'Expected 2 messages, got '.$mailThief->messages->count().'.');
        $mailThief->messages->each(function ($message, $key) use ($emailRecipients) {
            $this->assertEquals(1, $message->from->count());
            $this->assertEquals('noreply@enolalabs.com', $message->from->first());

            $this->assertEquals('This is a testing email!', $message->subject);

            $this->assertEquals(1, $message->to->count());
            $this->assertEquals($emailRecipients[$key], $message->to->first());

            $this->assertEquals(1, $message->bcc->count());
            $this->assertEquals($message->bcc->first(), 'hidden@enolalabs.com');

            $this->assertEquals(1, $message->cc->count());
            $this->assertEquals($message->cc->first(), 'copy@enolalabs.com');

            $this->assertTrue(
                $message->contains('Welcome '.$emailRecipients[$key].',<br /><br />'),
                'Expected to see "Welcome '.$emailRecipients[$key].',<br /><br />", but it was not there.'
            );
            $this->assertTrue(
                $message->contains('Thanks for signing up!<br /><br />'),
                'Expected to see "Thanks for signing up!<br /><br />", but it was not there.'
            );
            $this->assertTrue(
                $message->contains('Log into the application <a href="test.com">here</a>!'),
                'Expected to see "Log into the application <a href="test.com">here</a>!", but it was not there.'
            );
        });
    }
}
